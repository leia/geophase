include(FetchContent)

cmake_minimum_required(VERSION 3.13)

project(geophase   VERSION 1.0
                   DESCRIPTION "geophase: A C++ template library for geometrical operations on non-convex polyhedra with non-planar faces"
                   LANGUAGES CXX)
enable_testing()

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

if(MSVC)
    add_compile_options(/W4 /WX)
else(MSVC)
    set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -Wno-deprecated -Wno-attributes -Wno-unused-variable -Wno-unused-parameter -D_USE_MATH_DEFINES")
    set(CMAKE_CXX_FLAGS_TIMING "-O3 -std=c++2a ${CMAKE_CXX_FLAGS_RELEASE} -DTIMING")
    set(CMAKE_CXX_FLAGS_TIMINGINFO "${CMAKE_CXX_FLAGS_TIMING} -DCCS_OUTPUT -DCH_POLY_OUTPUT -DNEWTON_OUTPUT")
    set(CMAKE_CXX_FLAGS_GPROF "${CMAKE_CXX_FLAGS_RELEASE} -pg")
    set(CMAKE_CXX_FLAGS_HPCTOOLKIT "-std=c++2a -g -O3 -Wno-deprecated -Wno-attributes -Wno-unused-variable -Wno-unused-parameter -D_USE_MATH_DEFINES -DTIMING")
    set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -ggdb3 -O0 -Wall -Wextra -DDEBUG -pedantic -D_USE_MATH_DEFINES -DBOOST_MATH_INSTRUMENT")
    set(CMAKE_CXX_FLAGS_TIMINGOUTPUT "${CMAKE_CXX_FLAGS_TIMING} -DOUTPUT")
    set(CMAKE_CXX_FLAGS_RELWITHDEBINFO "${CMAKE_CXX_FLAGS_RELEASE} -ggdb3 -Wall -Wextra -pedantic -D_USE_MATH_DEFINES")
endif(MSVC)

# Add Google Test dependency
FetchContent_Declare(googletest
    GIT_REPOSITORY          https://github.com/google/googletest.git
    GIT_TAG                 release-1.8.1
    GIT_SHALLOW             YES
    GIT_PROGRESS            YES
    USES_TERMINAL_DOWNLOAD  YES
    USES_TERMINAL_UPDATE    YES
)

FetchContent_GetProperties(googletest)
if(NOT googletest_POPULATED)
    FetchContent_Populate(googletest)
    add_subdirectory(${googletest_SOURCE_DIR} ${googletest_BINARY_DIR})
endif()

# Require Eigen 
set(CMAKE_PREFIX_PATH "$ENV{EIGEN3_ROOT}")
find_package(Eigen3 REQUIRED)

# Configure the INTERFACE library 
add_library(geophase INTERFACE) 
target_include_directories(geophase INTERFACE 
    ${EIGEN3_INCLUDE_DIR}
    include/ 
    include/core/
    include/io/
    include/io/vtk/
    include/testing/
    include/geometrical_algorithm/
    include/geometrical_model/
    include/interface_positioning/
)
 
# Executables and tests

# Unit testing
add_executable(geophaseUnitTest app/test/geophaseUnitTest.cpp) 
target_link_libraries(geophaseUnitTest geophase gtest)
add_test(NAME unitTests COMMAND geophaseUnitTest)
set_tests_properties(unitTests PROPERTIES LABELS "UNIT_GEO")

# Perturbation 
add_executable(geophaseTestPerturb app/test/geophaseTestPerturb.cpp) 
target_link_libraries(geophaseTestPerturb geophase gtest)
add_test(NAME perturbationTests COMMAND geophaseTestPerturb)
set_tests_properties(perturbationTests PROPERTIES LABELS "UNIT_GEO")

# Inside tests 
add_executable(geophaseTestInside app/test/geophaseTestInside.cpp) 
target_include_directories(geophaseTestInside PUBLIC /usr/include/png++)
target_link_directories(geophaseTestInside PUBLIC /usr/lib)
target_link_libraries(geophaseTestInside geophase gtest png)
add_test(NAME insideOutsideTests COMMAND geophaseTestInside)
set_tests_properties(insideOutsideTests PROPERTIES LABELS "UNIT_GEO")

# Area and volume
add_executable(geophaseTestAreaVolume app/test/geophaseTestAreaVolume.cpp) 
target_link_libraries(geophaseTestAreaVolume geophase gtest)
add_test(NAME areaAndVolumeTests COMMAND geophaseTestAreaVolume)
set_tests_properties(areaAndVolumeTests PROPERTIES LABELS "UNIT_GEO")

# Extreme points 
add_executable(geophaseTestExtremePoints app/test/geophaseTestExtremePoints.cpp) 
target_link_libraries(geophaseTestExtremePoints geophase gtest)
add_test(NAME extremePointsTests COMMAND geophaseTestExtremePoints)
set_tests_properties(extremePointsTests PROPERTIES LABELS "UNIT_GEO")

# Triangulation
add_executable(geophaseTestTriangulation app/test/geophaseTestTriangulation.cpp) 
target_link_libraries(geophaseTestTriangulation geophase gtest)
add_test(NAME triangulationTests COMMAND geophaseTestTriangulation)
set_tests_properties(triangulationTests PROPERTIES LABELS "UNIT_GEO")

# Polygon orientation 
add_executable(geophaseTestOrient app/test/geophaseTestOrient.cpp) 
target_link_libraries(geophaseTestOrient geophase gtest)
add_test(NAME orientationTests COMMAND geophaseTestOrient)
set_tests_properties(orientationTests PROPERTIES LABELS "UNIT_GEO")

# Intersection 
add_executable(geophaseTestIntersect app/test/geophaseTestIntersect.cpp) 
target_link_libraries(geophaseTestIntersect geophase gtest)
add_test(NAME intersectionTests COMMAND geophaseTestIntersect)
set_tests_properties(intersectionTests PROPERTIES LABELS "UNIT_GEO")

# Interface positioning
add_executable(geophaseTestInterfacePositioning app/test/geophaseTestInterfacePositioning.cpp) 

## CCS Unit tests
target_link_libraries(geophaseTestInterfacePositioning geophase gtest)
add_test(NAME ccsUnitTest COMMAND geophaseTestInterfacePositioning --gtest_filter=UNIT*CONSECUTIVE*)
set_tests_properties(ccsUnitTest PROPERTIES LABELS "UNIT_CCS")

## CCS Convergence tests
add_test(NAME ccsConvergenceTest COMMAND geophaseTestInterfacePositioning --gtest_filter=CONVERGENCE*CONSECUTIVE*)
set_tests_properties(ccsConvergenceTest PROPERTIES LABELS "CONVERGENCE_CCS")


## NCS Unit tests
target_link_libraries(geophaseTestInterfacePositioning geophase gtest)
add_test(NAME ncsUnitTest COMMAND geophaseTestInterfacePositioning --gtest_filter=UNIT*NEWTON*)
set_tests_properties(ncsUnitTest PROPERTIES LABELS "UNIT_NCS")

## NCS Convergence tests
add_test(NAME ncsConvergenceTest COMMAND geophaseTestInterfacePositioning --gtest_filter=CONVERGENCE*NEWTON*)
set_tests_properties(ncsConvergenceTest PROPERTIES LABELS "CONVERGENCE_NCS")
