/*---------------------------------------------------------------------------*\

    geophase: A C++ template library for geometrical operations on non-convex
    polyhedra with non-planar faces
     
    Copyright (C) 2019 Tomislav Maric, TU Darmstadt 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

Description
    Main header file that includes all useful geometry files.  

Author
    Tomislav Maric maric@mma.tu-darmstadt.de

\*---------------------------------------------------------------------------*/

#ifndef Geometry_H
#define Geometry_H

// Core files

#include "Tolerance.H"

// Geometrical concepts

#include "LineIntersection.H"
#include "AABBox.H"
#include "PointSequence.H"
#include "PointSequenceIntersection.H"
#include "PolygonSequence.H"
#include "MixedPolyhedron.H"
#include "PolygonSequenceIntersection.H"
#include "ArrayTriangle.H"
#include "ArrayTetrahedron.H"
#include "TetrahedronSequence.H"
#include "TriangulationIntersection.H"
#include "InterfaceElementSequence.H"
#include "HalfspaceSequence.H"

// Predefined geometries 
#include "aabboxFwd.H"
#include "arrayTetrahedronFwd.H"
#include "arrayTriangleFwd.H"
#include "pointSequenceFwd.H"
#include "polygonSequenceFwd.H"
#include "tetrahedronSequenceFwd.H"
#include "triangleSequenceFwd.H"
#include "halfspaceSequenceFwd.H"

// Geometrical algorithms
// Not all algorithms are listed, as some are mutually dependent. 
#include "Perturb.H"
#include "Intersect.H"
#include "Area.H"
#include "EdgeTriangulate.H"
#include "Volume.H"
#include "CorrectVolume.H"
#include "Normal.H"
#include "WriteVtkPolydata.H"
#include "BuildFoam.H"
#include "Displace.H"
#include "FluxTriangulate.H"
#include "streamIO.H"

#endif

// ************************************************************************* //
