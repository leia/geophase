/*---------------------------------------------------------------------------*\

    geophase: A C++ template library for geometrical operations on non-convex
    polyhedra with non-planar faces
     
    Copyright (C) 2019 Tomislav Maric, TU Darmstadt 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

Description
    Helper functions for VTK IO file naming. 

Author
    Tomislav Maric maric@mma.tu-darmstadt.de

\*---------------------------------------------------------------------------*/

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam { namespace GeometricalTransport { 

template<typename P, typename ... Q>
void appendSuffix(std::stringstream& ss, P const & p, Q...q)
{
    ss << "-" << std::setfill('0') << std::setw(8) << p; 
    appendSuffix(ss, q...); 
}

template<typename...Q> 
std::string appendSuffix(const std::string& s, Q... q)
{
    // Stream the first string argument only once. 
    std::stringstream ss; 
    ss << s;  
    // Padd the other arguments and stream them.
    appendSuffix(ss, q...);
    return ss.str(); 
}

template<typename ...Q> 
std::string prependVtkFileName(const std::string & f, Q...q)
{
    std::stringstream ss; 
    // Stream the file name, pad the other arguments with 8 zeros. 
    ss << appendSuffix(f, q...); 
    // Append the .vtk extension.
    ss << ".vtk"; 
    return ss.str(); 
} 

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

}} // End namespace Foam::GeometricalTransport

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

