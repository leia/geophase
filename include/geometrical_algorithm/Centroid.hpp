/*---------------------------------------------------------------------------*\

    geophase: A C++ template library for geometrical operations on non-convex
    polyhedra with non-planar faces
     
    Copyright (C) 2019 Tomislav Maric, TU Darmstadt 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

Description
    Centroid calculation. 

Authors
    Tomislav Maric maric@mma.tu-darmstadt.de

\*---------------------------------------------------------------------------*/

#ifndef Centroid_HPP
#define Centroid_HPP

#include "tag.hpp"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace geophase { 

    template <typename PointSequence>
    std::enable_if_t
    <
        tag_base_of<PointSequence, point_sequence_tag>(), 
        typename PointSequence::value_type
    >
    centroid(PointSequence const & polygon)
    {
        typename PointSequence::value_type result = {};

        for (const auto& point : polygon)
            result += point; 

        result /= polygon.size(); 

        return result; 
    }

    template <typename Polyhedron>
    std::enable_if_t
    <   tag_base_of<Polyhedron, sequence_polyhedron_tag>(), 
        typename Polyhedron::value_type::value_type
    >
    centroid(Polyhedron const& poly)
    {
        typename Polyhedron::value_type::value_type result = {};

        for (const auto& polygon : poly)
            result += centroid(polygon); 

        result /= poly.size(); 

        return result;
    }

    //template
    //<
        //typename MixedPolyhedron 
    //>
    //typename std::enable_if
    //<
        //Traits::tag_enabled<MixedPolyhedron, mixed_polyhedron_tag>::value, 
        //typename MixedPolyhedron::PointType
    //>::type 
    //centroid(MixedPolyhedron const & mixedPoly)
    //{
        //typename MixedPolyhedron::PointType result(0,0,0); 

        //unsigned int centroidCount = 0; 

        //for (const auto& polygon : mixedPoly.polygons())
        //{
           //result += centroid(polygon);  
           //++centroidCount; 
        //}

        //for (const auto& triangle : mixedPoly.triangles())
        //{
            //result += centroid(triangle); 
            //++centroidCount; 
        //}

        //result /= centroidCount; 

        //return result;  
    //}


} // End namespace geophase 

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
