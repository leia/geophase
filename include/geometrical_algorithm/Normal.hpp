/*---------------------------------------------------------------------------*\

    geophase: A C++ template library for geometrical operations on non-convex
    polyhedra with non-planar faces
     
    Copyright (C) 2019 Tomislav Maric, TU Darmstadt 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

Description
    Normal vector calculation. 

Authors
    Tomislav Maric maric@mma.tu-darmstadt.de

\*---------------------------------------------------------------------------*/

#ifndef Normal_H 
#define Normal_H 

#include "tag.H"
#include "Triangulate.H"
#include "triangleSequenceFwd.H"
#include <vector>

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam { namespace GeometricalTransport { 

    //template
    //<
        //typename TriangulationStrategy = void, 
        //typename Triangle 
    //>
    //typename std::enable_if
    //<
        //Traits::tag_enabled<Triangle, triangle_tag>::value,
        //typename Triangle::value_type
    //>::type 
    //normal_area_vec(Triangle const & triangle)
    //{
        //return 0.5 * (
            //(triangle[1] - triangle[0]) ^ 
            //(triangle[2] - triangle[0])
        //); 
    //}

    //template
    //<
        //typename TriangulationStrategy = void, 
        //typename Triangulation 
    //>
    //typename std::enable_if
    //<
        //Traits::tag_enabled<Triangulation, triangle_collection_tag>::value,
        //std::vector<typename Triangulation::value_type::value_type>
    //>::type 
    //normal_area_vec(Triangulation const & triangulation)
    //{
        //std::vector
        //<
            //// Vertex/Point/Vector type.
            //typename Triangulation::value_type::value_type
        //> normals(triangulation.size()); 

        //decltype(triangulation.size()) i = 0; 
        //for (const auto& triangle : triangulation)
        //{
            //normals[i] = normal_area_vec(triangle); 
            //++i; 
        //}

        //return normals; 
    //}


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

}} // End namespace Foam::GeometricalTransport

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
