/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2012 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Description
    Displace algorithms.  

    Displaces the geometry based on the provided vector. 

Authors
    Tomislav Maric maric@csi.tu-darmstadt.de, tomislav@sourceflux.de
    Mathematical Modeling and Analysis
    Center of Smart Interfaces, TU Darmstadt

\*---------------------------------------------------------------------------*/

#ifndef Displace_H 
#define Displace_H 

#include <algorithm>

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam { namespace GeometricalTransport {

    template
    <
        typename PolygonSequence, 
        typename Vector
    >
    typename std::enable_if
    <
        Traits::tag_enabled<PolygonSequence, polygon_sequence_tag>::value, 
        PolygonSequence 
    >::type
    displace(
        PolygonSequence const & polyhedron, 
        Vector const & displacement
    )
    {
        PolygonSequence result(polyhedron);

        for (auto& polygon : result)
        {
            for (auto& point : polygon)
            {
                point += displacement;
            }
        }

        return result;
    } 

    template <typename PointCollectionOne, typename PointCollectionTwo>
    typename std::enable_if
    <
        Traits::tag_enabled<PointCollectionOne, point_collection_tag>::value &&
        Traits::tag_enabled<PointCollectionTwo, point_collection_tag>::value, 
        PointCollectionOne 
    >::type
    displace(PointCollectionOne const & polygon, PointCollectionTwo const & displacements)
    {
        PointCollectionOne result(polygon);

        auto it = displacements.begin();  
        for (auto& point : result)
            point += *it++;

        return result;
    } 

    template <typename PointCollection, typename Vector>
    typename std::enable_if
    <
        Traits::tag_enabled<PointCollection, point_collection_tag>::value &&
        Traits::tag_enabled<Vector, vector_tag>::value, 
        PointCollection
    >::type
    displace(PointCollection const & polygon, Vector const & displacement)
    {
        PointCollection displacements(polygon.size(), displacement);

        return displace(polygon, displacements);
    } 


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

}} // End namespace Foam::GeometricalTransport

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
