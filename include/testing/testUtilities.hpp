/*---------------------------------------------------------------------------*\

    geophase: A C++ template library for geometrical operations on non-convex
    polyhedra with non-planar faces
     
    Copyright (C) 2019 Tomislav Maric, TU Darmstadt 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


Description
    Utility functions that shorten access to Google Test test data. 

Authors
    Tomislav Maric maric@mma.tu-darmstadt.de

\*---------------------------------------------------------------------------*/

#ifndef testUtilities_HPP
#define testUtilities_HPP

#include "gtest/gtest.h" 
#include <numeric>
#include "tag.hpp"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// Global functions that get test info from Google Test framework.

auto testInfo()
{
    const ::testing::TestInfo* const test_info =
      ::testing::UnitTest::GetInstance()->current_test_info();

    return test_info; 
} 

auto fullTestName(char sep = '.')
{
    std::string testName = testInfo()->name(); 
    std::string testCaseName = testInfo()->test_case_name();
    return testCaseName + sep + testName; 
}

auto fullTestFileName(char sep = '.', std::string ext="vtk")
{
    return fullTestName(sep) + sep + ext;  
}

namespace geophase {

    template<typename T1, typename T2>
    bool are_equal_sets(T1 const & t1, T2 const & t2)
    {
        if (t1.size() != t2.size())
        {
            return false;  
        }

        bool equal = true; 

        for (auto e1 : t1)
        {
            if (find(t2.begin(), t2.end(), e1) == t2.end())
            {
                equal = false;  
            }
        }
        return equal; 
    }

    template<typename T1, typename T2>
    bool are_ordered_equal(T1 const & t1, T2 const & t2)
    {
        if (t1.size() != t2.size())
        {
            return false; 
        }

        auto  i1 = t1.begin(); 
        auto  i2  = t2.begin(); 

        for (;i1 < t1.end(); ++i1, ++i2)
        {
            if (*i1 != *i2)
            {
                return false; 
            }
        }
        return true;
    } 

} // End namespace geophase 

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
