/*---------------------------------------------------------------------------*\

    geophase: A C++ template library for geometrical operations on non-convex
    polyhedra with non-planar faces
     
    Copyright (C) 2019 Tomislav Maric, TU Darmstadt 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


Description
    Polygon intersection that wrapps the resulting intersection and the 
    intersection points.  

    Used for collection based polygon intersections - polygons that store
    their own points, instead of using indices to refer to some global
    point list.   


Author
    Tomislav Maric maric@mma.tu-darmstadt.de

\*---------------------------------------------------------------------------*/

#ifndef LineIntersection_H
#define LineIntersection_H

#include <vector>
#include <memory>
#include "tag.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam { namespace GeometricalTransport { 

/*---------------------------------------------------------------------------*\
                         Class LineIntersection Declaration
\*---------------------------------------------------------------------------*/

template<typename Point>  
class LineIntersection 
{

public:

    LineIntersection() = default; 
    LineIntersection(const LineIntersection&) = default; 
    ~LineIntersection() = default; 

    template<typename T>
    T& operator=(T const& p1)
    {
        point_ = p1;  
        return point_; 
    }

    void setValid() 
    {
        valid_ = true; 
    }

    bool isValid() const
    {
        return valid_; 
    }

    const Point& point() const
    {
        return point_; 
    }

private:

    Point point_; 

    bool valid_ = false; 
};

//namespace Traits
//{
    //template<typename Polygon>
    //struct tag <LineIntersection<Polygon>>
    //{
        //typedef line_intersection_tag type;
    //};

//// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

//} // End namespace Traits 

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

}} // End namespace Foam::GeometricalTransport

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
