/*---------------------------------------------------------------------------*\

    geophase: A C++ template library for geometrical operations on non-convex
    polyhedra with non-planar faces
     
    Copyright (C) 2019 Tomislav Maric, TU Darmstadt 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


Description
    A line connecting two points.  

Author
    Tomislav Maric maric@mma.tu-darmstadt.de

\*---------------------------------------------------------------------------*/

#ifndef LineSegment_H
#define LineSegment_H

#include "tag.hpp"
#include "constants.hpp"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace geophase { 

/*---------------------------------------------------------------------------*\
                         Class LineSegment Declaration
\*---------------------------------------------------------------------------*/

template<typename Point, typename Enabled=void> 
class LineSegment;

template<typename Point> 
class LineSegment<Point, enabled_if_tagged_t<Point, point_tag>>
{
    Point P1_ = Point(MAX, MAX, MAX); 
    Point P2_ = Point(MAX, MAX, MAX); 

public:

    typedef Point PointType;

    LineSegment() = default;

    LineSegment(
        Point const & P1,
        Point const & P2 
    )
    :
        P1_(P1), 
        P2_(P2)
    {}

    LineSegment(const LineSegment&) = default;

    Point const& firstPoint() const
    {
        return P1_; 
    }

    Point const& secondPoint() const
    {
        return P2_; 
    }

    // FIXME: Keep these, or drop them?
    template<typename PointType>
    void setFirstPoint(PointType const & p) 
    {
        P1_ = p; 
    }

    template<typename PointType>
    void setSecondPoint(PointType const & p) 
    {
        P2_ = p; 
    }

};

template<typename Point> 
struct tag<LineSegment<Point>>
{
    typedef line_segment_tag type;
};

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace geophase 

#endif

// ************************************************************************* //
