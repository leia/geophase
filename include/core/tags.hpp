/*---------------------------------------------------------------------------*\

    geophase: A C++ template library for geometrical operations on non-convex
    polyhedra with non-planar faces
     
    Copyright (C) 2019 Tomislav Maric, TU Darmstadt 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

Description
    Available geometry tags that show which geometrical models are available. 

Author
    Tomislav Maric maric@mma.tu-darmstadt.de

\*---------------------------------------------------------------------------*/

#ifndef tags_hpp
#define tags_hpp

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace geophase { 

    // Container tags
    struct direct_access_tag {};  

    // Vector and point 
    struct vector_tag {};  

    // Halfspace
    struct halfspace_tag {}; 

    // A sequence container of points.
    struct  point_sequence_tag {}; 

    // Polygon as a STL sequence of points. 
    struct polygon_tag {};  

    struct sequence_polygon_tag {}; 

    struct vector_polygon_tag 
        : point_sequence_tag, 
          sequence_polygon_tag, 
          direct_access_tag
    {};

    struct list_polygon_tag
        : point_sequence_tag, 
          sequence_polygon_tag
    {};

    struct deque_polygon_tag
        : 
            point_sequence_tag,
            sequence_polygon_tag, 
            direct_access_tag
    {};

    struct array_polygon_tag
        : 
            point_sequence_tag, 
            sequence_polygon_tag, 
            direct_access_tag 
    {};

    // Polyhedra as STL sequences of polygons.
    struct sequence_polyhedron_tag {}; 

    struct vector_polyhedron_tag 
        : sequence_polyhedron_tag, direct_access_tag
    {};

    struct list_polyhedron_tag
        : sequence_polyhedron_tag
    {};

    struct deque_polyhedron_tag
        : sequence_polyhedron_tag, direct_access_tag
    {};

    struct array_polyhedron_tag
        : sequence_polyhedron_tag, direct_access_tag 
    {};

    struct polygon_intersection_tag {};

    struct axis_box_tag {}; 

    // STL sequence tag, for storing models in STL containers. 
    struct sequence_tag {};

    //struct vector_collection_tag : point_collection_tag {}; 
    //struct point_sequence_tag : point_collection_tag {}; 
    //struct point_sequence_direct_tag : point_sequence_tag {}; 
    //struct line_segment_tag {}; 
    //struct ray_tag {}; 
    
    //struct halfspace_tag {};
    //struct halfspace_collection_tag {}; 
    //struct halfspace_sequence_tag : halfspace_collection_tag {};
    //struct halfspace_sequence_direct_tag : halfspace_sequence_tag {}; 


    //// Tag triangle as a direct sequence of points. 
    //struct triangle_tag {}; 
    //struct triangle_collection_tag {}; 
    //struct triangle_sequence_tag : triangle_collection_tag {}; 
    //struct triangle_sequence_direct_tag : triangle_sequence_tag {}; 
    //struct triangle_intersection_tag {};

    //struct polygon_tag : point_collection_tag {};
    //struct polygon_collection_tag {}; 
    //struct polygon_sequence_tag : polygon_collection_tag {};
    //struct polygon_sequence_direct_tag : polygon_sequence_tag {}; 
    //struct mixed_polyhedron_tag {};  

    //struct polygon_intersection_tag {}; 
    //struct indirect_polygon_tag {}; 

    //struct tetrahedron_tag : triangle_sequence_direct_tag {}; 
    //struct tetrahedron_collection_tag {};
    //struct tetrahedron_sequence_tag : tetrahedron_collection_tag {}; 
    //struct tetrahedron_sequence_direct_tag : tetrahedron_sequence_tag {}; 

    //struct indirect_polyhedron_tag {}; 
    //struct polyhedron_intersection_tag {}; 
    //struct triangulation_intersection_tag {};
    //struct double_polyhedron_tag {};  
    //struct pair_tag {}; 

    //struct geometrical_interface_tag {}; 

    //struct mesh_tag {};

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace geophase 

#endif

// ************************************************************************* //
