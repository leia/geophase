/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2013 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Application
    geomTestDistance 

Description
    Test application for the distance algorithm.

    Uses "Google Test" for automated testing. 

Authors
    Tomislav Maric maric@csi.tu-darmstadt.de, tomislav@sourceflux.de
    Mathematical Modeling and Analysis
    Center of Smart Interfaces, TU Darmstadt

\*---------------------------------------------------------------------------*/

#include "fvCFD.H"
#include "testUtilities.H"

#include <vector>
#include <algorithm>

#include "OFstream.H"
#include "Geometry.H"

#include <fstream>
#include <gtest/gtest.h> 

#include "Tolerance.H"

using namespace GeometricalTransport;

typedef Halfspace<point, vector> halfspace;

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
// Main program:

TEST(distanceTest, halfspacePointDistance) 
{
    auto tolerance = Traits::tolerance<scalar>::value();

    halfspace h1(point(0,0,0), vector(1,0,0)); 

    ASSERT_TRUE(distance(h1, point(0,0,0)) == 0); 
    ASSERT_TRUE(distance(h1, point(1,0,0)) == 1); 
    ASSERT_TRUE(distance(h1, point(0,1,0)) == 0); 

    ASSERT_TRUE(distance(point(0,0,0),h1) == 0); 
    ASSERT_TRUE(distance(point(1,0,0),h1) == 1); 
    ASSERT_TRUE(distance(point(0,1,0),h1) == 0); 

    halfspace h2(point(0.5,0.5, 0), vector(1,1,0)); 

    ASSERT_TRUE(distance(h2, point(1,0,0)) == 0); 
    ASSERT_TRUE(distance(h2, point(0,1,0)) == 0); 
    ASSERT_TRUE(
        mag(mag(distance(h2, point(1,1,0))) - 0.5*std::sqrt(2)) <=
        Traits::tolerance<scalar>::value()
    ); 
    ASSERT_LE(
        mag(mag(distance(h2, point(0,0,0))) - 0.5*std::sqrt(2)), 
        tolerance
    ); 
   
    halfspace h3(point(1,1, 0), vector(1,1,0)); 
    ASSERT_LE(
        mag(mag(distance(h3, point(0,0,0))) - Foam::sqrt(2.)), 
        tolerance
    );
}

int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();

    return 0;
}


// ************************************************************************* //
