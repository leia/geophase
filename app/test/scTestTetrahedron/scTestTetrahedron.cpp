/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2013 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Application
    geomTransportTestTetrahedronVolume 

Description
    Build a tetrahedron from 4 prescribed points: (0, 0, 0), (1 0 0), 
    (1,1,0) and (0, 1, 0).

    Write the tetrahedron out into "tetrahedron.vtk" file. 

    Compute and check the tetrahedron volume. 
    
Author
    Tomislav Maric maric@csi.tu-darmstadt.de

\*---------------------------------------------------------------------------*/

#include "fvCFD.H"

#include "Geometry.H"

#include "gtest/gtest.h" 

using namespace GeometricalTransport;

const double EPSILON = 1e-15; 

TEST(tetrahedron, volume)
{
    typedef ArrayTriangle<point> triangle;
    typedef ArrayTetrahedron<triangle> tetrahedron;

    // Build a tetrahedron. 
    auto tet = build<tetrahedron>(point(0,0,0), point(1,0,0), point(0,1,0), point(0,0,1));

    ASSERT_LE(mag(volume(tet) -  (1.0 / 6.0)), EPSILON); 
}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
// Main program:

int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();


    return 0;
}


// ************************************************************************* //
