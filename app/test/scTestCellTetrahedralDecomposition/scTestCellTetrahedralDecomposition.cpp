/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2013 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Application
    geomTestCellTetrahedralDecomposition 

Description
    Compute the volumes of all mesh cells by decomposing the cells into 
    tetrahedra. OpenFOAM does this already internally, but the algorithms 
    were necessary to implement for efficiency reasons.

Author
    Tomislav Maric maric@csi.tu-darmstadt.de

\*---------------------------------------------------------------------------*/

#include "fvCFD.H"

#include <vector>
#include <algorithm>

#include "Geometry.H"

#include <fstream>
#include "gtest/gtest.h" 

using namespace GeometricalTransport;

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
// Main program

TEST(cellDecomposition, volumeAndTopology)
{
    typedef PointSequence<point, std::vector> polygon; 
    typedef PolygonSequence<polygon, std::vector> polyhedron; 
    typedef ArrayTriangle<point> triangle;
    typedef ArrayTetrahedron<triangle> tetrahedron;
    typedef TetrahedronSequence<tetrahedron, std::vector> tetDecomposition;  

    extern int mainArgc; 
    extern char** mainArgv; 

    int argc = mainArgc; 
    char** argv = mainArgv; 

    #include "setRootCase.H"
    #include "createTime.H"
    #include "createMesh.H"

    const DimensionedField<scalar, volMesh>& V = mesh.V(); 

    forAll (V, cellI)
    {
        auto poly = build<polyhedron>(cellI, mesh); 

        auto cellDecomposition = barycentric_triangulate<tetDecomposition>(poly); 

        scalar decompositionVolume = volume(cellDecomposition); 

        ASSERT_TRUE(
            mag(decompositionVolume - V[cellI]) <= 
            Traits::tolerance<scalar>::value()
        ) << decompositionVolume << "!= " << V[cellI] << endl;
    }
}

int mainArgc; 
char** mainArgv; 

int main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);

    mainArgc = argc; 
    mainArgv = argv;

    return RUN_ALL_TESTS();
}


// ************************************************************************* //
