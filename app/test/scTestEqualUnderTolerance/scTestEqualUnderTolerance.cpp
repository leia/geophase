/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2013 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Application
    geomTestDistance 

Description
    Test application for the distance algorithm.

    Uses "Google Test" for automated testing. 

Authors
    Tomislav Maric maric@csi.tu-darmstadt.de, tomislav@sourceflux.de
    Mathematical Modeling and Analysis
    Center of Smart Interfaces, TU Darmstadt

\*---------------------------------------------------------------------------*/

#include "fvCFD.H"

#include "Geometry.H"

#include <gtest/gtest.h> 

using namespace GeometricalTransport;

// Global tolerance for the perturbation tests.
static double EPSILON = 1e-15; 
// Global maximal number of perturbations.
static unsigned int MAX_IT = 1e06;  

#include "testUtilities.H"
#include "streamIO.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
// Main program:

TEST(equalUnderTolerance, perturbedPoints) 
{
    point p1 (0,0,0);

    auto p1orig (p1);  

    auto equalUnderTolerance = EqualUnderTolerance(EPSILON); 

    for (unsigned int i = 0; i < MAX_IT; ++i)
    {
        perturb(p1, EPSILON);  

        bool equalPointsOne = equal_under_tolerance(p1, p1orig); 

        bool equalPointsTwo = equalUnderTolerance(p1, p1orig); 

        ASSERT_TRUE(equalPointsOne && equalPointsTwo)
            << "p1orig : " << p1orig << "\n" << "p1 : " << p1 << "\n"; 

        p1 = p1orig; 
    }
}

int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();

    return 0;
}


// ************************************************************************* //
