# geophase 

`geophase` is a C++ template library for geometrical operations on non-convex polyhedra with non-planar faces.

The `geophase` library implements elementary geometrical algorithms for the unstructured un-split Volume-of-Fluid method, used to simulate multiphase flows. Elementary geometrical algorithms are supported on **multiset models**. Multiset models are the simplest geometrical models because they do not store topological information of their sub-elements. For example, a polyhedron represented with a multiset model is a **polygon soup**: each polygon in the multiset stores its own data. 

Available algorithms: area, volume, and distance calculation, as well as triangulation, intersection and intersection tests. You can check `include/geometrical_algorithms` to see which algorithms are supported. 

## Authors

* **Tomislav Maric** - *Development, Continuous Integration* - [MMA, TU Darmstadt](https://www.mma.tu-darmstadt.de/index/mitarbeiter_3/mitarbeiter_details_mma_43648.en.jsp)
* **Tobias Tolle ** - *Continuous Integration* - [MMA, TU Darmstadt](https://www.mma.tu-darmstadt.de/index/mitarbeiter_3/mitarbeiter_details_mma_53952.en.jsp)
* **Ioannis Papagiannidis** - *Continuous Integration* 

## License

This project is licensed under the GPL3.0 License - see the [LICENSE.md](LICENSE.md) file for details.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

`geophase` is a header-only C++ template library. The file organization reflects what the library can do:

* Geometrical models are locatec in `geophase/include/geometrical_models`.
* Available elementary geometrical algorithms are located in `geophase/include/geometrical_models`.
* Algorithms related to the interface positioning for the VOF method are in `geophase/include/interface_positioning`.

Understanding how to use the library is easiest by observing the tests available in `app/test`. Each test application concerns a different set of algorithms and so a test application is a good starting point when trying to use `geophase`.

### Dependencies 

#### Singularity 

A [Singularity image](https://sylabs.io/docs/) containing all the dependencieas and the source-code is available at TUDatalib:  

To clone the code, execute 

```
./geophase.sif clone geophase
```

To build the source code on your machine using the dependencies from the image, execute 

```
./geophase.sif build geophase build
```

This builds the binaries in the `geophase/build` directory.

To run the tests, execute 

```
./geophase.sif run-tests geophase build
```

To view the test results with `jupyter notebook`, execute

```
./geophase.sif jupyter-notebook geophase
```

View the notebook by typing in the address `localhost:8889` in your web browser.

#### Building from sources

Versions of the dependencies used to build the code and generate the output data are placed in brackets, but other versions may work as well. 

List of prerequisites with tested versions in brackets:

* g++    (10.1.0)
* CMake  (3.18) 
* Eigen  (3.3.4.4)
* boost  (1.72.0-1)
* libpng (0.2.9-1)

List of prerequisites for the visualization of test data: 

* python (3.8)
	* python-pandas 
	* python-seaborn 
	* jupyter 

* texlive (PDF diagram generation) 

*Installing python environment using venv on a machine that doesn't support python 3.8*

1. Install python3.8-venv package on your operating system (venv = virtual environment)

2. Create a virtual environment folder on your machine with

```
	python3.8 -m venv my-virtual-env
```

3. Source the activation script to start using python 3.8


```
	. ./my-virtual-env/bin/activate
```

4. Use pip to install the above mentioned python prerequisites


```
	pip install pandas numpy matplotlib seaborn ipykernel
```

5. Install the new jupyter kernel 

```
        ipython kernel install --user --name=my-virtual-env
```

6. Start `jupyter notebook` in the `geophase/jupyter-notebooks` folder, click on `Kernel->Select Kernel` in the menu and select `my-virtual-env`. 

7. Restart the kernel to reproduce results from the tests.

### Installing

`geophase` is built using CMake. In the `geophase` directory, create a build directory

```
?> mkdir build
?> cd build
```

Configure `geophase` with `Timing` option to re-create test results from the publication:

```
?> cmake -DCMAKE_BUILD_TYPE=Timing ..
```

`geophase` is a header-only C++ template library, so only the test applications will be comopiled. Running `make` will install all the binaries of the test applications into the build directory

```
?> make 
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

CTest is used to generate and execute tests, to run all tests, execute 

```
?> ctest -V 
```

in the chosen build directory.

### Unit and convergence tests 

The tests consist of unit tests for the geometrical operations as well as the unit and convergence tests for the VOF interface positioning algorithm implemented in `geophase`. The geometrical unit tests are PASS / FAIL tests. The output of convergence tests can be examined using the `geophase-interface-positioning.ipynb` jupyter notebook available in `geophase/jupyter-notebooks`. 

After running the tests in the build directory run

```
?> cd ../notebooks
?> jupyter notebook `geophase-interface-positioning.ipynb`
```

This will process the test data and show results for interface positioning.  

## Using geophase in your code

To use `geophase` in your C++ software, you need to tag the types in your software that correspond to geophase models. For the OpenFOAM CFD software, this looks like  

```{.cpp}
namespace geophase { 

    // An OpenFOAM vector is a vector so tag it as a vector.
    template<>
    struct tag<Foam::vector>
    {
        using type = vector_tag;
    };

    // An OpenFOAM std::vector polygon is a VectorPolygon.
    using foamVectorPolygon = VectorPolygon<Foam::vector>; 
}

```

So an OpenFOAM vector is a vector, but for `geophase` to recognize it as such, it needs to be tagged as a vector. This is done by partially specializing the `tag` trait structure. Then, if you want to use the multiset polygon, a template alias is already available in `geophase` that is tagged as `vector_polygon`. This is a multiset polygon that uses the `vector` container from the STL and is always tagged as a `vector_polygon`.  

If you add the above code to your C++ OpenFOAM code, you can use `Foam::vector` with geophase arithmetics for vectors and `foamVectorPolygon` as a polygon with `geophase` algorithms.  

## Building a Singularity image 

Build the Singularity image:

```
sudo singularity build geophase.sif geophase.def 
```

### Optional: overlay a filesystem image for persistant storage

Create a filesystem image that will be used to save persistent data:

```
dd if=/dev/zero of=overlay.img bs=1M count=500 && mkfs.ext3 overlay.img
```

Add the filesystem overlay to the image:

```
singularity sif add --datatype 4 --partfs 2 --parttype 4 --partarch 2 --groupid 1 geophase.sif overlay.img
```

## Contributing

The code is maintained at [TU-GitLab](https://git.rwth-aachen.de/leia/geophase). Feedback in the form of contributions, bug reports or feature requests is handled there. Users external to the German TU-GitLab network can login using their github.com credentials. 

